import pandas as pd
pd.options.mode.chained_assignment = None
from excel2pd import dfFromExcel
from data2tex import *

def createCommand(title, author, institution, date_time_start,room,abstract,command_name):
    ti=text2tex(title)
    au=authors2tex(author,author_format="fN lN")
    inst=text2tex(institution)
    date,time=datetime2tex(date_time_start)
    room=text2tex(room)
    abstract=abstract2tex(abstract)
    return f"{command_name}{{{ti}}}{{{au}}}{{{inst}}}{{{date}}}{{{time}}}{{{room}}}{{{abstract}}}\n"


command_name="\\plenary"
output_name="PT_list.tex"

df=dfFromExcel('AIP2023_papers.xlsx',path_sessions='AIP2023_sessions.xlsx')
dfp=df[df["contribution_type"]=="Plenary lecture"]
dfp=dfp[dfp["session_short"]!="CD"]
dfp.sort_values(by=['actual_start'],axis=0,inplace=True)
dfp.reset_index(drop=True, inplace=True)

layout="""\\subsection*{\\marginpar{\\large{#4}\\\\ \\large{#5}\\\\\\large{#6}}#2}
%\\large {#3}
%
%\\vspace{\\baselineskip}
\\noindent\\large\\textbf{#1}
\\vspace{\\baselineskip}

\\noindent{#7}"""
command=f"\\newcommand{{{command_name}}}[7]{{{layout}}}\n\n"

with open(output_name,'w') as file:
    #file.write(command)
    for index, row in dfp.iterrows():
        file.write(createCommand(row['title'],row['authors'],row['organisations'],row['actual_start'],row['session_room'],row['abstract'],command_name))
print(f"{output_name} written successfully")

