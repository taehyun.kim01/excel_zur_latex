import datetime
import re
import pandas as pd

#FUNCTIONS FOR TEXCONVERSION
def formatAuthor(author,format="lN, fN"):
    if author:
        last_name,first_name=author.split(", ")
        #convert upper case name to lower case version:Commented out because it causes out of range errors for some authors
        #last_name = " ".join([p for parts in last_name.split(" ") for p in ["-".join([part[0]+"".join([x.lower() for x in part[1:]]) for part in parts.split("-")])]])
        if(format=="lN, fN"):
            return last_name+', '+first_name
        elif(format=="fN lN"):
            return first_name+" "+last_name
        elif(format=="abbr_fN lN"):
            return shortenName(first_name)+" "+last_name
        elif(format=="abbr1_fN lN"):
            return shortenName(first_name,only_first_letter=True)+" "+last_name
        elif(format=="abbr_fN abbr_lN" or format=="initials"):
            return shortenName(first_name)+" "+shortenName(last_name)
    else:
        return ""


def removeBrackets(string):
    bid=0
    new_string=""
    for s in string:
        if(s=="("):
            bid+=1
        if(bid==0):
            new_string+=s
        if(s==")"):
            bid=max(0,bid-1)
    if(new_string[-1]==" "):
        new_string=new_string[:-1]
    return new_string

def shortenName(name,only_first_letter=False):
    if(only_first_letter):
        return name[0]+"."
    shortened=""
    pname=name.replace("."," ").replace("  "," ")
    if(pname[-1]==" "):
        pname=pname[:-1]
    subnames=pname.split(" ")
    for sname in subnames:
        ssnames=sname.split("-")
        sabbr=""
        if(len(ssnames)>0):
            for j in range(len(ssnames)-1):
                sabbr+=ssnames[j][0]+".-"
            sabbr+=ssnames[len(ssnames)-1][0]+"."
        else:
            sabbr=ssnames[0][0]+"."
        shortened+=sabbr+" "
    return shortened[:len(shortened)-1]

def text2tex(plain_text):#Mostly for titles
    text=str(plain_text)
    text=text.replace('&','\\&')
    return replaceNonlatex(text)

def date2tex(plain_date,date_format='%a %b %d'):
    dt=datetime2tex(plain_date,date_format=date_format)
    if(dt==None):
        return dt
    return dt[0]

def time2tex(plain_date,time_format='%-I:%M %P'):
    dt=datetime2tex(plain_date,time_format=time_format)
    if(dt==None):
        return dt
    return dt[1]

def datetime2tex(plain_date,date_format='%a %b %-d',time_format='%-I:%M %P'):
    if(type(plain_date)==str):
        dts=re.split('-| |:',plain_date)
        if(len(dts)<5):
            print(f"Warning: {plain_date} is not a valid date/time")
            return None
        dt=datetime.datetime(*[int(d) for d in dts])
        return dt.strftime(date_format),dt.strftime(time_format)
    if(type(plain_date)==pd.Timestamp or type(plain_date)==datetime.datetime):
        return plain_date.strftime(date_format),plain_date.strftime(time_format)
    print(f"Warning: {plain_date} is not a valid date/time")
    return None

def date2weekday(plain_date):
    return date2tex(plain_date,date_format='%A')

def authors2tex(authors,presenting_author=None,highlight="\\underline",author_sep=',',author_format="abbr_fN lN",with_numbers=False):
    #Underlines presenting author and removes linebreaks. standard format is abbreviated firstname lastname see formatAuthor function for other formats
    underlined=str(authors)
    aList=[removeBrackets(author) for author in authors.split(';\n')]
    if(presenting_author!=None):
        if(type(presenting_author)is str):
            underlined = underlined.replace(presenting_author,highlight+"{"+presenting_author.strip(' ')+"}")
        else:
            print(f"Warning: presenting author is {str(presenting_author)} for {underlined}")
    for author in aList:
        underlined=underlined.replace(author,formatAuthor(author,author_format))
    underlined=underlined.replace(';',author_sep)
    if(with_numbers):
        underlined=underlined.replace(' (','$^{(').replace(')',')}$')
    else:
        underlined=removeBrackets(underlined)
    underlined=underlined.replace(" "+author_sep,author_sep)
    underlined=underlined.replace('\n',' ').strip()
    return replaceNonlatex(underlined)

def organisations2tex(organisations,org_format=("NR: ","ORG"), org_sep=";"):
    orgs=organisations.split(";\n")
    if(len(orgs)==1):
        return org_format[1].replace("ORG",orgs[0])
    org_string=""
    for org_ in orgs:
        nr, org=org_.split(": ")
        org_string+=org_format[0].replace("NR",nr)
        org_string+=org_format[1].replace("ORG",org)
        org_string+=org_sep+" "
    return org_string[:len(org_string)-1-len(org_sep)]

def abstract2tex(abstract):
    parts=re.split('(?=\[1\] [A-Z].)',abstract)
    if(len(parts)==1):#Quickfix for finish link problem
        parts=re.split('(?=\[1\] www.)',abstract)
    if(len(parts)==2):
        refs=re.split('\[[0-9]+\] ',parts[1])
        pure_refs=[refs[i].strip('\n').strip(' ') for i in range(1,len(refs))]
        ref_items=""
        for ref in pure_refs:
            ref_items+='\\item '+ref+'\n'
        ref_string="\\begin{references}\n"+ref_items+"\\end{references}"
        ref_string=ref_string.replace('&','\\&')
        return replaceNonlatex(parts[0].strip('\n').strip(' ')+'\n\n'+ref_string)
    elif(len(parts)!=1):
        print('Warning: Could not uniquely determine start of references')
    abstract=abstract.strip('\n').strip(' ')
    return replaceNonlatex(abstract)

def getchairs(chair1,chair2,chair3):
    '''
    chairi format: Surname, Name Name ...
    return L. L. ... Surname, l. l. Surname ...
    '''
    return ', '.join(filter(None, [formatAuthor(chair1,"abbr_fN lN"),formatAuthor(chair2,"abbr_fN lN"),formatAuthor(chair3,"abbr_fN lN")]))

def shortSessionNamenNice(session_short):
    '''
    comes in format [A-Z]{2}\d{2} \d*
    '''
    match = re.match(r"(?P<LetterTag>[A-Z]{2})(?P<MSNumber>\d{2})(?: (?P<Number>\d*)|)",session_short)
    if match.group("Number"):
        return  match.group("LetterTag")+" "+match.group("MSNumber")+"-"+match.group("Number")
    else:
        return  match.group("LetterTag")+" "+match.group("MSNumber")

def replaceNonlatex(text):
    ###Unknown character replacements
    text=text.replace('\u00f8','\\o ')# o with slash
    text=text.replace('\u0107','\\\'c')# c with accent
    text=text.replace('\u012d','\\u{\\i}')# i with curve
    text=text.replace('\xa0', ' ')# weird free space
    text=text.replace('\u200b','')# invisible space
    text=text.replace('\ufeff','')# invisible space
    text=text.replace('\u2010','-')# short line
    text=text.replace('\u2212','-')# short line
    text=text.replace('\u2013','-')# short line
    text=text.replace('\u2014','--')# long line
    text=text.replace('\u2018','`')# left opening `
    text=text.replace('\u2019','\'')# right closing '
    text=text.replace('\u201c','``')# left opening `
    text=text.replace('\u201d','\'\'')# right closing '
    text=text.replace('\u00df','\\ss ')#german sharp s (esszett)
    ###Specific changes
    text=text.replace('lug &','lug \\&')#& in Plug & play
    text=text.replace('0%','0\\%')#% in 30% and 80%
    text=text.replace('https://doi.org/10.4213/rm10080','\\\\https://doi.org/10.4213/rm10080')#Novikov
    text=text.replace('https://epubs.siam.org/doi/10.1137/16M110183','\\\\https://epubs.siam.org/doi/10.1137/16M110183')#Raik
    text=text.replace('https://dx.doi.org/10.1190/geo2019-0527.1', '\\\\https://dx.doi.org/10.1190/geo2019-0527.1')#Faucher
    text = re.sub(r'\$[ ]*?\\textit(?P<name>.*?\})[ ]*?\$',r' \\emph\g<name>',text)

    '''
    if(text_alt!=text):
        with open('changed.txt','a',encoding="utf-8") as file:
            file.write(text_alt)
            file.write('\nNew text:\n')
            file.write(text+'\n')
    '''
    return text
#TESTS
'''
print(shortenName("Emil-F. Marko-Paskal-Qualle Tom"))

import pandas as pd
from excel2pd import dfFromExcel

df=dfFromExcel('AIP2023_papers.xlsx',path_sessions='AIP2023_sessions.xlsx')
#print(df.dtypes)

for index, row in df.iterrows():
    if(row['paperID']==539):
        #date,time=datetime2tex(row['session_start'])
        #print(date)
        #print(time)
        #date,time=datetime2tex(row['actual_end'])
        #print(time)
        #print(row['authors'])
        #print(row['presenting_author'])
        #print(authors2tex(row['authors'],row['presenting_author'],with_numbers=False))
        #print(organisations2tex(row['organisations']))
        #print('NEXT')
        print(replaceNonlatex(row['abstract']))
'''
