import pandas as pd

#Main function
def dfFromExcel(path_papers,path_sessions=None,without_proposals=True,ret_sessions=False):
    '''
    returns dataframe for papers from path_papers (and for sessions if ret_sessions is set to true)
    '''
    df=pd.read_excel(path_papers)
    #disregard unimportant columns
    df=df[['paperID', 'contribution_type', 'authors', 'organisations',
        'title', 'keywords', 'topics',
       'abstract', 'abstract_plain', 'abstract2', 'abstract_plain2',
       'bibliography',  'submitting_author', 'sa_organisation',
       'sa_country', 'sa_region', 'presenting_author', 'session_ID', 'session_numberInSession', 'session_short',
       'session_title', 'session_info', 'session_start', 'session_end',
       'presentation_start', 'presentation_end', 'chair1',
       'chair1_organisation', 'chair2', 'chair2_organisation', 'chair3',
       'chair3_organisation', 'session_room_ID', 'session_room',
       'session_room_info', 'withdrawn','acceptance']]
    df=df[df['acceptance']!='Rejected']
    df=df[df['acceptance']!='On Hold']
    df=df.drop('acceptance', axis=1)
    df=df[df['withdrawn']==0]
    df=df.drop('withdrawn', axis=1)
    if(without_proposals):
        df=df[df['contribution_type']!='Proposal for a Minisymposium']
    #calculate talk times via information from sessions table
    if(path_sessions!=None):
        dfms=dfFromExcelSessions(path_sessions)
        df['presentation_nr']=df.apply(lambda row: calcTalkNrs(row,dfms), axis=1)
        df['actual_start']=df.apply(lambda row: calcTalkTimes(row), axis=1)
        df['actual_end']=df.apply(lambda row: calcTalkTimes(row,start=False), axis=1)
        if(ret_sessions):
            return df, dfms
    return df

def dfmsFromExcel(path_sessions,path_papers,ret_all=False):
    '''
    Returns dataframes:
    Parallel sessions as ParSes
    Poster sessions as PostSes (if ret_all=True)
    Plenary lectures as Pl (if ret_all=True)
    Rest as dfms (if ret_all=True)
    and fetshes also the presenting author for each presentation in Poster and Parallel Session
    '''
    dfms = pd.read_excel(path_sessions)
    # get all the relavent columns
    dfms = dfms[['session_ID','session_short','session_title', 'session_start','session_end',
        'session_room','chair1','chair2','chair3','presentations',
        'p1_paperID','p1_authors','p1_title','p1_presenting_author',
        'p2_paperID','p2_authors','p2_title','p2_presenting_author',
        'p3_paperID','p3_authors','p3_title','p3_presenting_author',
        'p4_paperID','p4_authors','p4_title','p4_presenting_author',
        'p5_paperID','p5_authors','p5_title','p5_presenting_author',
        'p6_paperID','p6_authors','p6_title','p6_presenting_author',
        'p7_paperID','p7_authors','p7_title','p7_presenting_author',
        'p8_paperID','p8_authors','p8_title','p8_presenting_author',
        'p9_paperID','p9_authors','p9_title','p9_presenting_author',
        'p10_paperID','p10_authors','p10_title','p10_presenting_author',
        'p11_paperID','p11_authors','p11_title','p11_presenting_author',
        'p12_paperID','p12_authors','p12_title','p12_presenting_author',
        'p13_paperID','p13_authors','p13_title','p13_presenting_author',
        'p14_paperID','p14_authors','p14_title','p14_presenting_author',
        'p15_paperID','p15_authors','p15_title','p15_presenting_author',
        'p16_paperID','p16_authors','p16_title','p16_presenting_author',
        'p17_paperID','p17_authors','p17_title','p17_presenting_author',
        'p18_paperID','p18_authors','p18_title','p18_presenting_author',
        'p19_paperID','p19_authors','p19_title','p19_presenting_author',
        'p20_paperID','p20_authors','p20_title','p20_presenting_author',
        'p21_paperID','p21_authors','p21_title','p21_presenting_author',
        'p22_paperID','p22_authors','p22_title','p22_presenting_author',
        'p23_paperID','p23_authors','p23_title','p23_presenting_author',
        'p24_paperID','p24_authors','p24_title','p24_presenting_author',
        'p25_paperID','p25_authors','p25_title','p25_presenting_author']]

    ParSes = dfms[dfms['session_short'].str.contains('^(?:MS|CT)', regex= True, na=False)]
    ParSes = ParSes[['session_short','session_title', 'session_start','session_end','session_room','chair1','chair2','chair3','presentations',
        'p1_presenting_author','p2_presenting_author','p3_presenting_author','p4_presenting_author']]
    values = {}
    for i in range(0,int(ParSes['presentations'].max())):
        values["p{:d}_presenting_author".format(i+1)] = ""
    for i in range(0,3):
        values["chair{:d}".format(i+1)] = ""
    ParSes = ParSes.fillna(value = values)
    if ret_all:
        dfms = dfms[~dfms['session_short'].str.contains('^(MS|CT)\d+', regex= True, na=False)]
        Poster = dfms[dfms['session_short'].str.contains('^P$', regex= True, na=False)]
        dfms = dfms[~dfms['session_short'].str.contains('^P$', regex= True, na=False)]
        Pl = dfms[dfms['session_short'].str.contains('^Pl \d+', regex= True, na=False)]
        dfms = dfms[~dfms['session_short'].str.contains('^Pl \d+', regex= True, na=False)]
        return ParSes, Poster, Pl, dfms
    else:
        return ParSes


def dfFromExcelSessions(path_sessions):
    df=pd.read_excel(path_sessions)#TODO trim down information maybe remove unnecessary entries
    return df


###Help functions for calculations of talk times
def getTalkNumber(session_ID,paper_ID,msframe):
    msrow=msframe[msframe['session_ID']==session_ID]
    totalTalks=int(msrow['presentations'].iloc[0])
    for i in  range(totalTalks):
        if(int(msrow[f"p{i+1}_paperID"].iloc[0])==paper_ID):
            return i+1
    print(f"Talk with ID {paper_ID} not found in session {session_ID}")
    return -1


def calcTalkNrs(row,msframe):
    if(row['contribution_type']!='Invited Minisymposium talk'and row['contribution_type']!='Contribution for an oral presentation'):
        return 1
    else:
        tN=getTalkNumber(int(row['session_ID']),int(row['paperID']),msframe)
        #if(tN>0):
        #    date,time=string2Time(str(row['session_start']))
        return tN

def calcTalkTimes(row,start=True):
    if(row['contribution_type']!='Invited Minisymposium talk' and row['contribution_type']!='Contribution for an oral presentation'):
        if(start):
            return row['session_start']
        return row['session_end']
    else:
        tN=row['presentation_nr']
        if(tN>0):
            if(start):
                return  row['session_start']+pd.Timedelta(minutes=30*(tN-1))
            return row['session_start']+pd.Timedelta(minutes=30*tN)
        print(f"Warning invalid presentation number: {tN}")
        return tN

###EXAMPLE FOR USAGE  PRINTS AUTHORS IN ALPHABETICAL ORDER
'''
df=dfFromExcel('AIP2023_papers.xlsx',path_sessions='AIP2023_sessions.xlsx')
#print(df.head())

df.sort_values(by=['authors'],axis=0,inplace=True)
df.reset_index(drop=True, inplace=True)
lt=[]
for index, row in df.iterrows():
    lt.append(row['contribution_type'])
    if(row['contribution_type']=='Contribution for an oral presentation'):
        print(row['actual_start'])
        print(row['actual_end'])
print(set(lt))
'''
#cells can be accessed via df['columnname'][index] BUT some indices may be missing
#so iteration as above is recommended
#cells can be filtered via df_filtered=df[df['columnname']condition]
#look at pandas documentation for further usage
