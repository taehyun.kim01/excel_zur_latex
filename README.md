# AIP Book of Abstract

This is only a short documentation and the person reding this should look at each file in detail. 

The tools provided here are meant to extract information from the files `AIP2023_papers.xlsx` and `AIP2023_sessions.xlsx` to make a nice book of abstracts in the file `main.tex` which then can be compiled with `LaTeX`. To do that there are four files:
 - `CTMS_list.py` creating `CTMS_list.tex` this file contains each contributed talk and mini symposia talk in alphabetical order. Each od that is wrapped into a `LaTeX` command `\contribution` defined in `main.tex`.
 - `MS_list.py` creates `MS_list.tex` which contains the overview over each mini symposium and has links to each abstract each such mini symposium is wrapped in a `MiniSym` environment defined in `main.tex` and each session in it is wrapped in a `MSSession` environment. 
 - `P_list.py` created `P_list.tex` is the file containing all the posters from the poster session and there abstracts each poster is created by the `\poster` command defined in `main.tex`
 - `PT_list.py` creates `PT_list.tex` which contains each plenary talk put into a `\plenary` command defined in `main.tex`

The main file then imports the three files and defines the rest of the styling. All of the above python scripts rely on helper functions in `data2tex.py` making it easy to transform given data to nice tex and `excel2pd.py` to create `pandas.DataFrame` from the given Excel files.  