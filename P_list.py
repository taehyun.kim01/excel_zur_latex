import pandas as pd
pd.options.mode.chained_assignment = None
from excel2pd import dfFromExcel
from data2tex import *

def createCommand(title, author, presenting_author, date_time_start,room,abstract,command_name):
    ti=text2tex(title)
    au=authors2tex(author,presenting_author)
    #inst=text2tex(institution)
    date,time=datetime2tex(date_time_start)
    room=text2tex(room)
    abstract=abstract2tex(abstract)
    return f"{command_name}{{{ti}}}{{{au}}}{{{date}}}{{{time}}}{{{room}}}{{{abstract}}}\n"


command_name="\\poster"
output_name="P_list.tex"
'''
import os
dirname = os.path.dirname(__file__)
f1 = os.path.join(dirname, 'AIP2023_papers.xlsx')
f2=os.path.join(dirname, 'AIP2023_sessions.xlsx')
on=os.path.join(dirname, output_name)
'''

df=dfFromExcel('AIP2023_papers.xlsx',path_sessions='AIP2023_sessions.xlsx')
dfp=df[df["session_short"]=="P"]
#dfp=df
dfp.sort_values(by=['presenting_author'],axis=0,inplace=True)
dfp.reset_index(drop=True, inplace=True)

layout="""\\subsection*{\\marginpar{\\large{#3}\\\\ \\large{#4}\\\\\\large{#5}}#1}
\\nopagebreak
\\large\\textbf{#2}

\\nopagebreak
\\vspace{\\baselineskip}
\\nopagebreak
\\noindent{#6}"""
command=f"\\newcommand{{{command_name}}}[6]{{{layout}}}\n\n"

with open(output_name,'w',encoding="utf-8") as file:
    #file.write(command)
    for index, row in dfp.iterrows():
        file.write(createCommand(row['title'],row['authors'],row['presenting_author'],row['actual_start'],row['session_room'],row['abstract'],command_name))
print(f"{output_name} written successfully")
