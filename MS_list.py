from data2tex import *
from excel2pd import *
import pandas as pd

df=dfFromExcel('AIP2023_papers.xlsx',path_sessions='AIP2023_sessions.xlsx')

def getOrganizers(session_data):
    # formate the chirs of the MS
    chairs = []
    for i in range(1,4):
        chair = session_data[f'chair{i}'].values[0]
        if type(chair) is str:
            chair = authors2tex(chair)
            chairs.append(chair)
    return ', '.join(chairs)

# def Author_shortend(author):
#     # shortens the name to first Letter
#     surname, name = author.split(',')
#     return name[1] + '. ' + surname

def write_session_header(MS,MS_S,session_data):
    file.write(r'\MSHeader')
    file.write('{' + MS + '}'+ '\n')#MS
    file.write('{' + text2tex(session_data['session_title'].values[0]) + '}'+ '\n')#Title
    file.write('{' + MS + '}'+ '\n')#
    file.write('{' + getOrganizers(session_data) + '}'+ '\n')

def write_session_contributions(session,session_data,file):
    # writes out the data of the session and the talks in it
    #session_start_data = pd.Timestamp(session_data['session_start'].values[0])
    #session_start = date2weekday(session_start_data) + ' ' + date2tex(session_start_data) + ' ' + time2tex(session_start_data)
    session_start_date,session_start_time=datetime2tex(pd.Timestamp(session_data['session_start'].values[0]))
    session_start = session_start_date + ' ' + session_start_time
    session_end = time2tex(pd.Timestamp(session_data['session_end'].values[0]))
    session_room = session_data['session_room'].values[0]  #can we get the geo data link for that?
    file.write(r'\begin{minipage}{\textwidth}'+ '\n')
    file.write(r'\sessionHeader')
    file.write('{'+ session +'}' + '\n')
    file.write('{'+ session_start + ' - ' + session_end +'}' + '\n')
    file.write('{'+ session_room +'}' + '\n')
    file.write(r'\sessionList{')
    for id in session_data['paperID'].values:
            file.write(r'\sessionListItem')
            presentation_data = session_data[session_data['paperID']==id]
            presentation_title = text2tex(presentation_data['title'].values[0])
            presentation_authors = authors2tex(presentation_data['authors'].values[0],presentation_data['presenting_author'].values[0])
            presentation_start = time2tex(pd.Timestamp(presentation_data['actual_start'].values[0]))
            presentation_end = time2tex(pd.Timestamp(presentation_data['actual_end'].values[0]))
            file.write(r'{' +presentation_title +r'}' + '\n')
            file.write('{' + presentation_authors +'}' + '\n')
            file.write('{' + presentation_start + r'}')
            file.write('{' + presentation_end   + r'}'+ '\n' )
            file.write('{' + f'{id}' + '}'+ '\n') #
    file.write(r'}' + '\n')
    file.write(r'\end{minipage}' + '\n')

def write_session(MS,multiple,df,file):
    if multiple:
        k = 1
        MS_S = MS + ' 1'
    else:
        k = 0
        MS_S = MS
    session_data = df[df['session_short']==MS_S]
    file.write("\\begin{{MiniSym}}{{{shorthandle}}}{{{title}}}{{{label}}}{{{chairs}}}\n".format(
        shorthandle = MS,
        title = text2tex(session_data['session_title'].values[0]),
        label = MS,
        chairs = getOrganizers(session_data)
    ))
    # if multiple:
    #     k = 1
    #     MS_S = MS + f' {k}'
    while MS_S in df['session_short'].values:
        session_data = df[df['session_short']==MS_S]
        session_data = session_data.sort_values(by='actual_start', inplace = False)
        if multiple:
            file.write("\\begin{{MSSession}}[{Number}]{{{time}}}{{{room}}}\n".format(
                Number = k,
                time = pd.Timestamp(session_data['actual_start'].values[0]).strftime('%a %b %-d, %-I:%M %P')+" --  "+pd.Timestamp(session_data['actual_end'].values[-1]).strftime('%-I:%M %P'),
                room = session_data['session_room'].values[0]
            ))
        else:
            file.write("\\begin{{MSSession}}{{{time}}}{{{room}}}\n".format(
                time = pd.Timestamp(session_data['actual_start'].values[0]).strftime('%a %b %-d, %-I:%M %P')+" --  "+pd.Timestamp(session_data['actual_end'].values[-1]).strftime('%-I:%M %P'),
                room = session_data['session_room'].values[0]
            ))
        for index, row in session_data.iterrows():
            file.write("  \\Presentation{{{title}}}{{{authors}}}{{{timeStart}}}{{{timeEnd}}}{{{refLink}}}\n".format(
                title = text2tex(row['title']),
                authors = authors2tex(row['authors'],row['presenting_author']),
                timeStart = pd.Timestamp(row['actual_start']).strftime('%-I:%M %P'),
                timeEnd = pd.Timestamp(row['actual_end']).strftime('%-I:%M %P'),
                refLink = row['paperID']
            ))
        file.write("\\end{MSSession}\n")
        k += 1
        MS_S = MS + f' {k}'
    # else:
    #     MS_S = MS
    #     session_data = df[df['session_short']==MS_S]
    #     file.write("\\begin{{MSSession}}{{{time}}}{{{room}}}\n".format(
    #         time = pd.Timestamp(session_data['session_start'].values[0]).strftime('%a %b %w %H:%M')+"-"+pd.Timestamp(session_data['session_end'].values[0]).strftime('%H:%M'),
    #         room = session_data['session_room'].values[0]
    #     ))
    #     for id in session_data['paperID'].values:
    #         presentation_data = session_data[session_data['paperID']==id]
    #         file.write("\\Presentation{{{title}}}{{{authors}}}{{{timeStart}}}{{{timeEnd}}}{{{refLink}}}\n".format(
    #             title = text2tex(presentation_data['title'].values[0]),
    #             authors = authors2tex(presentation_data['authors'].values[0],presentation_data['presenting_author'].values[0]),
    #             timeStart = pd.Timestamp(presentation_data['actual_start'].values[0]).strftime('%H:%M'),
    #             timeEnd = pd.Timestamp(presentation_data['actual_end'].values[0]).strftime('%H:%M'),
    #             refLink = id
    #         ))
    #     file.write("\\end{MSSession}\n")
    file.write("\\end{MiniSym}\n\n")

with open('MS_list.tex','w') as file:
    # file.write(r'\newcommand{\MSHeader}[4]{\subsection*{#1: #2}\label{#3} #4\\}')# MS**,Title,label,chairs
    # file.write(r'% MS**,Title,label,header' +'\n')
    # file.write(r'\newcommand{\sessionHeader}[3]{\vspace{2mm}\begin{minipage}[t]{0.94\textwidth}\textbf{#1:} #2\end{minipage}\begin{minipage}[t]{13mm}  \hfill \end{minipage}\begin{minipage}[t]{2cm}\raggedright\large #3 \end{minipage}}')# title,time,room
    # file.write(r'% Title,time,room' +'\n')
    # file.write(r'\newcommand{\sessionList}[1]{\begin{itemize} #1 \end{itemize}}')# listentries
    # file.write(r'% listentries' +'\n')
    # file.write(r'\newcommand{\sessionListItem}[5]{\item[]\begin{minipage}[t]{0.94\textwidth}\hyperref[ID:#5]{\textbf{#1}}\\ #2 \end{minipage}\begin{minipage}[t]{5mm}  \hfill \end{minipage}\begin{minipage}[t]{2cm}\raggedright\large #3 -\\ #4  \end{minipage}}')#title, authors, time start,time stop, link
    # file.write(r'% Title,authors,time start, time stop,link' +'\n')
    MS_number = 1
    MS = 'MS' + str(MS_number).zfill(2)
    for MS_number in range(1,100):
        MS = 'MS' + str(MS_number).zfill(2)
        if MS in df['session_short'].values:
            # MS with only one session
            # MS_S = MS
            write_session(MS,False,df,file)
            # session_data = df[df['session_short']==MS_S]
            # write_session_header(MS,MS_S,session_data)
            # session = r'Session'
            # write_session_contributions(session,session_data,file)

        elif MS + ' 1' in df['session_short'].values:
            # MS with more then one session
            write_session(MS,True,df,file)
            # k = 1
            # MS_S = MS + f' {k}'
            # session_data = df[df['session_short']==MS_S]
            # write_session_header(MS,MS_S,session_data)
            # while MS_S in df['session_short'].values:
            #     session = r'Session '+ f'{k}'
            #     session_data = df[df['session_short']==MS_S]
            #     write_session_contributions(session,session_data,file)
            #     k += 1
            #     MS_S = MS + f' {k}'

print("MS_list.tex written successfully")
