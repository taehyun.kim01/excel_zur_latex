import pandas as pd
pd.options.mode.chained_assignment = None
from excel2pd import dfFromExcel
from data2tex import *
import re

def createCommand(title, author, presenting_author, date_time_start,date_time_end,room,abstract,session,label_talk,command_name):
    ti=text2tex(title)
    au=authors2tex(author,presenting_author)
    time_end = time2tex(date_time_end)
    date,time_start=datetime2tex(date_time_start)
    session=text2tex(session)
    session_label=session.split(" ")[0]
    if(session[:2]=="MS"):
        session=f"\\hyperref[{session_label}]{{\\textbf{{{session}}}}}"
    else:
        session=f"\\textbf{{{session}}}"
    room=text2tex(room)
    abstract=abstract2tex(abstract)
    label_talk=f"ID:{label_talk}"
    return f"{command_name}{{{ti}}}{{{au}}}{{{date}}}{{{time_start}}}{{{time_end}}}{{{room}}}{{{abstract}}}{{{session}}}{{{label_talk}}}\n"

def alphLastName(name):
    name_split=re.split('(?=[A-Z])',name)
    if(len(name_split)<2):
        print(f"Warning {name} not a valid name!")
        return name
    else:
        return name_split[1]
    
def alphName(row):
    return alphLastName(str(row['presenting_author']))

command_name="\\contribution"
output_name="CTMS_list.tex"


'''
import os
dirname = os.path.dirname(__file__)
f1 = os.path.join(dirname, 'AIP2023_papers.xlsx')
f2=os.path.join(dirname, 'AIP2023_sessions.xlsx')
on=os.path.join(dirname, output_name)
'''

df=dfFromExcel('AIP2023_papers.xlsx',path_sessions='AIP2023_sessions.xlsx')

dfx = df[df["contribution_type"]!="Plenary lecture"]
dfp = dfx[dfx["session_short"] != "P"]
dfp['alphLastName']=dfp.apply(lambda row: alphName(row), axis=1)

#dfp=df
dfp.sort_values(by=['alphLastName'],axis=0,inplace=True)
dfp.reset_index(drop=True, inplace=True)
layout="""\\subsection*{\\marginpar{\\large{#8}\\\\\\large{#3}\\\\\\large{#4}\\\\\\large{#5}\\\\\\large{#6}}#1}\\label{#9}
\\nopagebreak
\\large\\textbf{#2}

\\nopagebreak
\\vspace{\\baselineskip}
\\nopagebreak
\\noindent{#7}"""

command=f"\\newcommand{{{command_name}}}[9]{{{layout}}}\n\n"

with open(output_name,'w',encoding="utf-8") as file:
    #file.write(command)
    for index, row in dfp.iterrows():
        file.write(createCommand(row['title'],row['authors'],row['presenting_author'],row['actual_start'],row['actual_end'],row['session_room'],row['abstract'],row['session_short'],row['paperID'],command_name))
print(f"{output_name} written successfully")
