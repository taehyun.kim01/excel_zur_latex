from excel2pd import *
from data2tex import *
import datetime as dt

### File to create Sessions.tex

#ParSes is still sorted by starting point and number of MS
ParSes = dfmsFromExcel('AIP2023_sessions.xlsx','AIP2023_papers.xlsx')

### Format to create:
#\begin{ParallelSession}[0.9em][Boolean to print head]{number of session}{Weekday, HH:MM-HH:MM}
#  \Session[Speeker 1][Speeker 2][Speeker 3][speeker 4][chair1, Chair 2, Chair 3]{Name of MS or CT}{Room}
#   ....
#\end{ParallelSession}
Bheader = True
chairpraefix = ""

pre_time = dt.datetime.today()
fileallcsv = open("./SessionsCSV/Sessions.csv", "w")
fileallcsv.write("Start;End;Short Name;Sesion Name;Chairs;Room; \n")
with open('Sessions.tex','w') as file:
    session_nr = 0
    for index, row in ParSes.iterrows():
        fileallcsv.write("{start};{end};{short};{SesName};{chairs};{room}; \n".format(short = row['session_short'], SesName = row['session_title'],chairs = getchairs(row["chair1"],row["chair2"],row["chair3"]),room = row['session_room'],start = row['session_start'].strftime('%A %d.%m. %H:%M'), end = row['session_end'].strftime('%A %d.%m. %H:%M')))
        pre_session_nr = session_nr
        if pre_time<row['session_start']:
            session_nr += 1
            if session_nr>1:
                file.write("\\end{ParallelSession} \n \n")
            if Bheader:
                file.write("\\begin{{ParallelSession}}[0.9em]{{Parallel Session {:d}}}{{{}-{}}} \n".format(
                    session_nr,
                    row['session_start'].strftime('%A %H:%M'),
                    row['session_end'].strftime('%H:%M')
                ))
            else:
                file.write("\\begin{{ParallelSession}}[0.9em][\\BooleanFalse]{{Parallel Session {:d}}}{{{}-{}}} \n".format(
                    session_nr,
                    row['session_start'].strftime('%A %H:%M'),
                    row['session_end'].strftime('%H:%M')
                ))
            pre_time = row['session_start']

        #open CSV for session
        if pre_session_nr != session_nr:
            if session_nr>1:
                f.close()
            f = open("./SessionsCSV/Session{:d}.csv".format(session_nr), "w")
        if row['session_short'].startswith('MS'):
            chairpraefix = "organized by "
        else:
            chairpraefix = "Chair: "
        #write into latex file
        file.write("\\Session[{spe1}][{spe2}][{spe3}][{spe4}][{chairs}]{{{SesName}}}{{{room}}} \n".format(
            spe1 = formatAuthor(row['p1_presenting_author'],"abbr_fN lN"),
            spe2 = formatAuthor(row['p2_presenting_author'],"abbr_fN lN"),
            spe3 = formatAuthor(row['p3_presenting_author'],"abbr_fN lN"),
            spe4 = formatAuthor(row['p4_presenting_author'],"abbr_fN lN"),
            chairs = chairpraefix+getchairs(row["chair1"],row["chair2"],row["chair3"]),
            SesName = shortSessionNamenNice(row['session_short'])+" "+row['session_title'],
            room = row['session_room']
        ))
        # write CSV
        #line with title
        f.write("{SesName};;;; \n".format(SesName = row['session_title']))
        #line with chairs and room number
        f.write("{chairs};;;{room}; \n".format(chairs = getchairs(row["chair1"],row["chair2"],row["chair3"]),room = row['session_room']))
        #line with presenting authors
        f.write("{spe1};{spe2};{spe3};{spe4}; \n".format(
            spe1 = formatAuthor(row['p1_presenting_author'],"abbr_fN lN"),
            spe2 = formatAuthor(row['p2_presenting_author'],"abbr_fN lN"),
            spe3 = formatAuthor(row['p3_presenting_author'],"abbr_fN lN"),
            spe4 = formatAuthor(row['p4_presenting_author'],"abbr_fN lN")
        ))
    file.write("\\end{ParallelSession} \n \n")

f.close()
